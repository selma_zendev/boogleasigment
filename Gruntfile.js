module.exports = function (grunt) {

    pkg: grunt.file.readJSON('./package.json'),

        // Project configuration.
        grunt.initConfig({
            jshint: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: [
                    'app/app.module.js',
                    'app/**/*.js',
                    'app/**/**/*.js'
                ]
            },
            jasmine: {
                customTemplate: {
                    src: [
                        "./node_modules/angular/angular.js",
                        "./node_modules/angular-cookies/angular-cookies.js",
                        "./node_modules/angular-route/angular-route.js",
                        './node_modules/angular-mocks/angular-mocks.js',
                        "./node_modules/angular-animate/angular-animate.js",
                        "./node_modules/@uirouter/angularjs/release/angular-ui-router.js",
                        "./app/app.module.js",
                        "./app/blocks/router/router.module.js",
                        "./app/core/core.module.js",
                        "./app/home/home.module.js",
                        "./app/game/game.module.js",
                        "./app/core/core.route.js",
                        "./app/blocks/router/route-helper.provider.js",
                        "./app/core/config.js",
                        "./app/core/rules.service.js",
                        "./app/core/player.service.js",
                        "./app/game/game.service.js",
                        "./app/home/home.route.js",
                        "./app/home/home.controller.js",
                        "./app/game/game.route.js",
                        "./app/game/game.controller.js",
                        "./app/game/board.directive.js",
                    ],
                    options: {
                        specs: 'app/**/*spec.js'
                    }
                }
            },
            html2js: {
                options: {
                    singleModule: true,
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: false,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    }
                },
                main: {
                    src: './app/**/**.html',
                    dest: './dist/templates.min.js'
                }
            },
            sass: {
                dist: {
                    options: {
                        style: 'compressed'
                    },
                    files: {
                        './dist/app.min.css': ['./sass/main.scss']
                    }
                }
            },
            uglify: {
                your_target: {
                    options: {
                        mangle: false
                    },
                    files: {
                        './dist/app.min.js': [
                            "./node_modules/angular/angular.js",
                            "./node_modules/angular-cookies/angular-cookies.js",
                            "./node_modules/angular-route/angular-route.js",
                            "./node_modules/angular-animate/angular-animate.js",
                            "./node_modules/@uirouter/angularjs/release/angular-ui-router.js",
                            "./app/app.module.js",
                            "./app/blocks/router/router.module.js",
                            "./app/core/core.module.js",
                            "./app/home/home.module.js",
                            "./app/game/game.module.js",
                            "./app/core/core.route.js",
                            "./app/blocks/router/route-helper.provider.js",
                            "./app/core/config.js",
                            "./app/core/rules.service.js",
                            "./app/core/player.service.js",
                            "./app/game/game.service.js",
                            "./app/home/home.route.js",
                            "./app/home/home.controller.js",
                            "./app/game/game.route.js",
                            "./app/game/game.controller.js",
                            "./app/game/board.directive.js",
                        ]
                    }
                }
            },
            concat: {
                basic: {
                    src: [
                        "./node_modules/angular/angular.js",
                        "./node_modules/angular-cookies/angular-cookies.js",
                        "./node_modules/angular-route/angular-route.js",
                        "./node_modules/angular-animate/angular-animate.js",
                        "./node_modules/@uirouter/angularjs/release/angular-ui-router.js",
                        "./app/app.module.js",
                        "./app/blocks/router/router.module.js",
                        "./app/core/core.module.js",
                        "./app/home/home.module.js",
                        "./app/game/game.module.js",
                        "./app/core/core.route.js",
                        "./app/blocks/router/route-helper.provider.js",
                        "./app/core/config.js",
                        "./app/core/rules.service.js",
                        "./app/core/player.service.js",
                        "./app/game/game.service.js",
                        "./app/home/home.route.js",
                        "./app/home/home.controller.js",
                        "./app/game/game.route.js",
                        "./app/game/game.controller.js",
                        "./app/game/board.directive.js",
                    ],
                    dest: './dist/app.js'
                }
            },
            watch: {
                css: {
                    files: ['sass/*.scss', 'sass/**/*.scss'],
                    tasks: ['sass']
                },
                js: {
                    files: [
                        './app/*.js',
                        './app/**/*.js',
                        './app/**/**/*.js',
                        '!./app/**/*.spec.js'
                    ],
                    tasks: ['jshint', 'concat', 'uglify', 'watch']
                },
                html: {
                    files: './app/**/**.html',
                    tasks: ['html2js:main']
                }
            }
        });

    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-jasmine');

    // Default task(s).
    grunt.registerTask('default', ['jshint', 'html2js', 'sass', 'concat', 'uglify', 'watch']);
    grunt.registerTask('build', ['jshint', 'html2js', 'sass', 'concat', 'uglify']);

};