/* Tests for "Word List Score function" part and "Multiplayer Score function" */

var wl = [];
wl['pluto'] = true; wl['gardyloo'] = true; wl['fuzzle'] = true; wl['sialoquent'] = true;  wl['lollygag'] = true;
wl['colly'] = true; wl['taradiddle'] = true; wl['widdershins'] = true; wl['gardy'] = true;
wl['bumfuzzle'] = true; wl['wabbit'] = true; wl['flibbertigibbe'] = true; wl['t'] = true; wl['collywobbles'] = true;
wl['nudiustertian'] = true; wl['teabag'] = true; wl['bloviate'] = true; wl['xertz'] = true; wl['mars'] = true; wl['catty'] = true;
wl['erinaceous'] = true; wl['wampus'] = true; wl['cattywampus'] = true; wl['gubbins'] = true;
wl['bibble'] = true; wl['loo'] = true; wl['snickersnee'] = true; wl['quire'] = true; wl['am'] = true; wl['malarkey'] = true; wl['help'] = true;

describe("checking for one player Word List Score function", function () {

    var rulesService;

    beforeEach(
        module('app.core')
    );

    beforeEach(inject(function (_rulesService_) {
        rulesService = _rulesService_;
        rulesService.setWordList(wl);
    }));

    it('should exist rulesService', function () {
        expect(rulesService).toBeDefined();
    });

    it('should exist rulesService.getScore', function () {
        expect(rulesService.getScore).toBeDefined();
    });

    it('should exist rulesService.resetUniqueWords', function () {
        expect(rulesService.resetUniqueWords).toBeDefined();
    });

    it('should exist rulesService.setPlayerCount', function () {
        expect(rulesService.setPlayerCount).toBeDefined();
    });

    it('should exist rulesService.setWordList', function () {
        expect(rulesService.setWordList).toBeDefined();
    });

    it("should return score '0' for bellow three letter word 'am'", inject(function (rulesService) {
        //rulesService.populateWordList(wl);
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("am")).toBe(0);
    }));

    it("should return score '1' for three letter word 'loo'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("loo")).toBe(1);
    }));

    it("should return score '1' for four letter word 'help'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("help")).toBe(1);
    }));

    it("should return score '2' for five letter word 'catty'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("catty")).toBe(2);
    }));

    it("should return score '3' for six letter word 'teabag'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("teabag")).toBe(3);
    }));

    it("should return score '5' for seven letter word 'gubbins'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("gubbins")).toBe(5);
    }));

    it("should return score '11' for eight letter word 'gardyloo'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("gardyloo")).toBe(11);
    }));

    it("should return score '11' for ten letter word 'taradiddle'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore("taradiddle")).toBe(11);
    }));

    it("should return score '1' for words 'loo, loo'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore(['loo', 'loo'])).toBe(1);
    }));

    it("should return score '34' for words 'am, loo, help, catty, teabag, gubbins, gardyloo, taradiddle'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore(['am', 'loo', 'help', 'catty', 'teabag', 'gubbins', 'gardyloo', 'taradiddle'])).toBe(34);
    }));

    it("should return score '80' for words as string 'catty,wampus,am,bumfuzzle,gardyloo,taradiddle,loo,snickersnee,widdershins,teabag,collywobbles,gubbins'", inject(function (rulesService) {
        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(1);
        expect(rulesService.getScore('catty,wampus,am,bumfuzzle,gardyloo,taradiddle,loo,snickersnee,widdershins,teabag,collywobbles,gubbins')).toBe(80);
    }));
});

describe("checking for Multiplayer Score function", function () {
    var rulesService;

    beforeEach(
        module('app.core')
    );

    beforeEach(inject(function (_rulesService_) {
        rulesService = _rulesService_;
        rulesService.setWordList(wl);
    }));

    it('should exist rulesService', function () {
        expect(rulesService).toBeDefined();
    });

    it('should exist rulesService.getPlayersScore', function () {
        expect(rulesService.getPlayersScore).toBeDefined();
    });

    it('should exist rulesService.resetUniqueWords', function () {
        expect(rulesService.resetUniqueWords).toBeDefined();
    });

    it('should exist rulesService.setPlayerCount', function () {
        expect(rulesService.setPlayerCount).toBeDefined();
    });

    it("should return score 0 beacause no player has unique words", inject(function (rulesService) {

        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(2);

        var playersWords = [
            {
                name: 'Lucas',
                words: ['am', 'bibble', 'loo', 'malarkey', 'nudiustertian', 'quire',
                    'widdershins', 'xertz', 'bloviate', 'pluto']
            },
            {
                name: 'Clara',
                words: ['am', 'bibble', 'loo', 'malarkey', 'nudiustertian', 'quire',
                    'widdershins', 'xertz', 'bloviate', 'pluto']
            }
        ];

        var players = rulesService.getPlayersScore(playersWords);

        for (var i = 0; i < players.length; i++) {
            var player = players[i];

            if (player.name == 'Lucas') {
                expect(player.score).toBe(0);
                continue;
            }

            if (player.name == 'Clara') {
                expect(player.score).toBe(0);
                continue;
            }
        }
    }));

    it("should return correct scores for 'Example input for 5 players'", inject(function (rulesService) {

        rulesService.resetUniqueWords();
        rulesService.setPlayerCount(5);

        var playersWords = [
            {
                name: 'Lucas',
                words: ['am', 'bibble', 'loo', 'malarkey', 'nudiustertian', 'quire',
                    'widdershins', 'xertz', 'bloviate', 'pluto']
            },
            {
                name: 'Clara',
                words: ['xertz', 'gardyloo', 'catty', 'fuzzle', 'mars', 'sialoquent',
                    'quire', 'lollygag', 'colly', 'taradiddle', 'snickersnee', 'widdershins', 'gardy']
            },
            {
                name: 'Klaus',
                words: ['bumfuzzle', 'wabbit', 'catty', 'flibbertigibbe', 't', 'am',
                    'loo', 'wampus', 'bibble', 'nudiustertian', 'xertz']
            },
            {
                name: 'Raphael',
                words: ['bloviate', 'loo', 'xertz', 'mars', 'erinaceous', 'wampus',
                    'am', 'bibble', 'cattywampus']
            },
            {
                name: 'Tom',
                words: ['bibble', 'loo', 'snickersnee', 'quire', 'am', 'malarkey']
            }
        ];

        var players = rulesService.getPlayersScore(playersWords);

        for (var i = 0; i < players.length; i++) {
            var player = players[i];

            if (player.name == 'Lucas') {
                expect(player.score).toBe(2);
                continue;
            }

            if (player.name == 'Clara') {
                expect(player.score).toBe(51);
                continue;
            }

            if (player.name == 'Klaus') {
                expect(player.score).toBe(25);
                continue;
            }

            if (player.name == 'Raphael') {
                expect(player.score).toBe(22);
                continue;
            }

            if (player.name == 'Tom') {
                expect(player.score).toBe(0);
                continue;
            }
        }
    }));
});