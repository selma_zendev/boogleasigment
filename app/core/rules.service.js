(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('rulesService', rulesService);

    rulesService.$inject = ['$http', 'gameService'];
    /* @ngInject */
    function rulesService($http, gameService) {

        var uniqueWords = [],
            playerCount = 1,
            wordList = [];

        var service = {
            getScore: getScore,
            getPlayersScore: getPlayersScore,
            resetUniqueWords: resetUniqueWords,
            setPlayerCount: setPlayerCount,
            setWordList: setWordList,
            populateWordList: populateWordList
        };

        return service;

        ///////////////

        function getScore(words) {

            var score = 0;

            /* In case of invalid input return score 0 */
            if (typeof words === 'undefined' || words.length === 0) {
                return score;
            }

            /* In case we don't get an array of words transform to array or return score immediately */
            if (!angular.isArray(words)) {

                var splittedWords = words.split(',');

                if(!splittedWords.length) {
                    return _getWordPoints(words);
                }

                words = splittedWords; 
            }

            /* In case player has submited same word more than once */
            words = words.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });

            /* Go through every word and count total score */
            for (var i = 0; i < words.length; i++) {
                score += _getWordPoints(words[i]);
            }

            return score;
        }

        function getPlayersScore(playersData) {

            /* First we need to populate unique words because they are the only ones that can be scored */
            var playersScore = [];

            uniqueWords = _getUniqueWords(playersData);

            /* Loop through every player and calculate total score */
            for (var i = 0; i < playersData.length; i++) {

                var player = playersData[i];

                if (typeof player === 'undefined') {
                    continue;
                }

                playersScore.push({
                    name: player.name,
                    score: getScore(player.words)
                });

            }

            return playersScore;
        }

        /* Needs to be execute on every new game start */
        function resetUniqueWords() {
            uniqueWords = [];
        }

        function setPlayerCount(number) {
            playerCount = number;
        }

        function setWordList(list) {
            wordList = list;
        }

        function _getUniqueWords(playersData) {
            var unique = [];

            /* Concat all words together so we can find only unique words */
            for (var i = 0; i < playersData.length; i++) {
                unique = unique.concat(playersData[i].words);
            }

            return _unique(unique);

            ///////////////

            /* Removes duplicates completely from array
            * https://stackoverflow.com/questions/11474422/deleting-both-values-from-array-if-duplicate-javascript-jquery
            */
            function _unique(arr) {
                var counts = arr.reduce(function (counts, item) {
                    counts[item] = (counts[item] || 0) + 1;
                    return counts;
                }, {});
                return Object.keys(counts).reduce(function (arr, item) {
                    if (counts[item] === 1) {
                        arr.push(item);
                    }
                    return arr;
                }, []);
            }
        }

        /* Returns word score based on word length: 
            - 3-4 chars = 1 point,
            - 5 chars = 2 points,
            - 6 chars = 3 points,
            - 7 chars = 5 points,
            - 8+ chars = 11 points
        */
        function _getWordPoints(word) {

            var wordLength = word.length;

            if (_invalidWord(word) || wordLength < 3) {
                return 0;
            }

            if (wordLength < 5) {
                return 1;
            }

            if (wordLength < 6) {
                return 2;
            }

            if (wordLength < 7) {
                return 3;
            }

            if (wordLength < 8) {
                return 5;
            }

            return 11;
        }

        function _invalidWord(word) {

            var inWordlist = typeof wordList[word.toLowerCase()] !== 'undefined';

            if (playerCount === 1 && inWordlist) {
                return false;
            }

            if (uniqueWords.length && uniqueWords.indexOf(word) !== -1 && inWordlist) {
                return false;
            }

            return true;
        }

        function populateWordList() {
            
            return $http.get(_getLocalizedWordList())
                .then(success)
                .catch(fail);

            function success(response) {

                var allLines = response.data.split(/\r\n|\n/),
                    list = [];
                // Reading line by line
                allLines.map(function(line){
                    list[line.toLowerCase().trim()] = true;
                });
                
                setWordList(list);

                return response;
            }

            function fail(e) {
                console.log('e', e);
            }
        }

        function _getLocalizedWordList() {
            
            var lang = gameService.getLanguage(),
                path = 'dist/wordlists/';

            if(lang === 'en') {
                return path + 'wordlist-english.txt';
            }

            if(lang === 'de') {
                return path + 'wortliste-deutsch.txt';
            }

            if(lang === 'fr') {
                return path + 'listedemots-francais.txt';
            }
        }
    }

})();
