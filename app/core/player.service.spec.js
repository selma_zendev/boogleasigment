describe("checking for player service", function () {

    var playerService;
    var wl = [];
    wl['loo'] = true, wl['am'] = true, wl['xertz'] = true;

    beforeEach(
        module('app.core')
    );

    beforeEach(inject(function (_playerService_, _rulesService_) {
        playerService = _playerService_;
        rulesService = _rulesService_;
        rulesService.setWordList(wl);
    }));

    it('should exist playerService', function () {
        expect(playerService).toBeDefined();
    });

    it('should exist playerService.setActivePlayerId', function () {
        expect(playerService.setActivePlayerId).toBeDefined();
    });

    it('should exist playerService.getActivePlayerId', function () {
        expect(playerService.getActivePlayerId).toBeDefined();
    });
    
    it('should exist playerService.setPlayers', function () {
        expect(playerService.setPlayers).toBeDefined();
    });

    it('should exist playerService.setPlayerScore', function () {
        expect(playerService.setPlayerScore).toBeDefined();
    });

    it('should return false when activePlayer not set', function () {
        expect(playerService.getActivePlayerId()).toBe(false);
    });

    it('should set activePlayerId correctly', function () {
        playerService.setActivePlayerId('Kenan');
        expect(playerService.getActivePlayerId()).toBe('Kenan');
    });

    it('should set players when input is only string', function () {
        playerService.setPlayers('Kenan');
        expect(playerService.getPlayer('Kenan').name).toBe('Kenan');
    });

    it('should set players when input is an array', function () {
        playerService.setPlayers(['Kenan', 'Hasan']);
        expect(playerService.getPlayer('Kenan').name).toBe('Kenan');
        expect(playerService.getPlayer('Hasan').name).toBe('Hasan');
    });

    it('should return active player object for getActivePlayer()', function () {
        playerService.setPlayers('Kenan');
        playerService.setActivePlayerId('Kenan');
        expect(playerService.getActivePlayer().name).toBe('Kenan');
    });

    it('should set player score to 1 for words "loo, am" and to have 2 words on player object', function () {
        playerService.setPlayers('Kenan');
        playerService.setPlayerScore(playerService.getPlayer('Kenan'), 'loo');
        playerService.setPlayerScore(playerService.getPlayer('Kenan'), 'am');

        expect(playerService.getPlayer('Kenan').score).toBe(1);
        expect(playerService.getPlayer('Kenan').words.length).toBe(2);
    });

    it('should set player score to 2 for word "xertz" and to have 1 words on player object', function () {
        playerService.setPlayers('Kenan');
        playerService.setPlayerScore(playerService.getPlayer('Kenan'), 'xertz');

        expect(playerService.getPlayer('Kenan').score).toBe(2);
        expect(playerService.getPlayer('Kenan').words.length).toBe(1);
    });

});