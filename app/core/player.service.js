(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('playerService', playerService);

    playerService.$inject = ['$rootScope', 'rulesService'];
    /* @ngInject */
    function playerService($rootScope, rulesService) {

        var _players = [],
            _activePlayerId = false;

        var service = {
            setPlayers: setPlayers,
            getPlayer: getPlayer,
            setActivePlayerId: setActivePlayerId,
            getActivePlayerId: getActivePlayerId,
            getActivePlayer: getActivePlayer,
            setPlayerScore: setPlayerScore,
            resetPlayerScore: resetPlayerScore
        };

        return service;

        ///////////////

        function setPlayers(players) {
            if (!angular.isArray(players)) {
                _players[players] = {
                    name: players,
                    words: [],
                    score: 0
                };
                return;
            }

            for (var i = 0; i < players.length; i++) {
                var player = players[i];
                _players[player] = {
                    name: player,
                    words: [],
                    score: 0
                };
            }
        }

        function getPlayer(name) {

            if (typeof _players[name] === 'undefined') {
                return false;
            }

            return _players[name];
        }

        function setActivePlayerId(name) {
            _activePlayerId = name;
            $rootScope.ActivePlayer = _activePlayerId;
        }

        function getActivePlayerId() {
            return _activePlayerId;
        }

        function getActivePlayer() {
            return getPlayer(_activePlayerId);
        }

        function setPlayerScore(player, word) {

            var currentPlayer = getPlayer(player.name),
                wordScore = rulesService.getScore(word);

            if (typeof currentPlayer.words === 'undefined') {
                currentPlayer.words = [];
            }

            currentPlayer.words.push({
                word: word,
                score: wordScore
            });

            currentPlayer.score += wordScore;

            setPlayers(currentPlayer);
        }

        function resetPlayerScore(player) {

            var currentPlayer = getPlayer(player.name);
            
            currentPlayer.words = [];
            currentPlayer.score = 0;

            setPlayers(currentPlayer);
        }
    }
})();
