(function () {
    'use strict';

    angular
        .module('app.game')
        .controller('GameController', GameController);

    GameController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout',
        'playerService', 'rulesService', 'gameService'];
    /* @ngInject */
    function GameController($scope, $rootScope, $state, $stateParams, $timeout,
        playerService, rulesService, gameService) {
        var vm = this;
        vm.title = 'Game';

        activate();

        function activate() {

            if ($stateParams.language) {
                gameService.setLanguage($stateParams.language);
            }

            rulesService.populateWordList();

            var players = $stateParams.players;

            if (typeof players === 'undefined' || !players.length) {
                $state.go('home');
            }

            if (!playerService.getPlayer(players)) {

                playerService.setPlayers(players);
                playerService.setActivePlayerId(players);
            }

            $scope.player = playerService.getActivePlayer();

            $scope.$on('playerUpdated', function () {
                $scope.player = playerService.getActivePlayer();
            });

            $scope.$on('newGame', function () {
                $scope.gameFinished = false;
                $scope.countdown = '03:00';
                $scope.player = playerService.getActivePlayer();

                startTimer();
            });

            var countdownTimeout;
            $scope.gameFinished = false;
            $scope.countdown = '03:00';

            startTimer();

            return 'Activated game View';

            ///////////////

            // https://codepen.io/ishanbakshi/pen/pgzNMv
            function startTimer() {
                var presentTime = $scope.countdown;
                var timeArray = presentTime.split(/[:]+/);
                var m = timeArray[0];
                var s = checkSecond((timeArray[1] - 1));
                if (s === 59) { m = m - 1; }
                if (m < 0) { window.alert('timer completed'); }

                $scope.countdown = m + ':' + s;

                if ($scope.countdown === '00:00') {
                    $scope.gameFinished = true;
                    $timeout.cancel(countdownTimeout);
                    return;
                }

                countdownTimeout = $timeout(startTimer, 1000);
            }

            function checkSecond(sec) {
                if (sec < 10 && sec >= 0) { sec = '0' + sec; } // add zero in front of numbers < 10
                if (sec < 0) { sec = '59'; }
                return sec;
            }
        }
    }
})();
