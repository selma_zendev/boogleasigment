(function () {
  'use strict';

  angular
    .module('app.game')
    .directive('bglBoard', bglBoard);

  bglBoard.$inject = ['playerService', 'gameService'];
  /* @ngInject */
  function bglBoard(playerService, gameService) {

    var directive = {
      templateUrl: 'app/game/board.html',
      restrict: 'E',
      link: link
    };

    return directive;

    function link(scope, element, attr) {

      var boardDice = gameService.generateBoardDice();

      scope.boardDice = boardDice;
      scope.currentWord = '';
      scope.onDiceClick = onDiceClick;
      scope.submitWord = submitWord;
      scope.newGame = newGame;

      ///////////////

      function onDiceClick(dice) {

        if (dice.disabled || dice.used) {
          return;
        }

        disableDice(dice);

        scope.currentWord += dice.char;
      }

      function disableDice(dice) {

        var availableCoords = gameService.calcAvailableCoords(dice);

        scope.boardDice.firstRow.applyDisable(dice, availableCoords);
        scope.boardDice.secondRow.applyDisable(dice, availableCoords);
        scope.boardDice.thirdRow.applyDisable(dice, availableCoords);
        scope.boardDice.forthRow.applyDisable(dice, availableCoords);
      }

      function submitWord() {

        if (scope.$parent.gameFinished) {
          window.alert('Game finished.');
          return;
        }

        if (!scope.currentWord.length || scope.currentWord.length < 3) {
          window.alert('Word must be at least 3 chars');
          return;
        }

        var activePlayer = playerService.getActivePlayer();

        if (isWordDuplicateForPlayer(activePlayer.words)) {
          window.alert('Duplicate word');
          return;
        }

        playerService.setPlayerScore(activePlayer, scope.currentWord);
        scope.currentWord = '';

        scope.boardDice.firstRow.applyEnableAll();
        scope.boardDice.secondRow.applyEnableAll();
        scope.boardDice.thirdRow.applyEnableAll();
        scope.boardDice.forthRow.applyEnableAll();

        scope.$emit('playerUpdated');
      }

      function isWordDuplicateForPlayer(words) {

        for (var i = 0; i < words.length; i++) {
          if (words[i].word.toLowerCase() === scope.currentWord.toLowerCase()) {
            return true;
          }
        }

        return false;
      }

      function newGame() {
        scope.currentWord = '';

        var boardDice = gameService.generateBoardDice();
        scope.boardDice = boardDice;
        playerService.resetPlayerScore(playerService.getActivePlayer());

        scope.$emit('newGame');
      }

    }
  }
})();
