describe("checking for game service", function () {

    var gameService;

    beforeEach(
        module('app.core')
    );

    beforeEach(inject(function (_gameService_) {
        gameService = _gameService_;
    }));

    it('should exist gameService', function () {
        expect(gameService).toBeDefined();
    });

    it('should exist gameService.generateBoardDice', function () {
        expect(gameService.generateBoardDice).toBeDefined();
    });

    it('should exist gameService.calcAvailableCoords', function () {
        expect(gameService.calcAvailableCoords).toBeDefined();
    });

    it('should exist gameService.setLanguage', function () {
        expect(gameService.setLanguage).toBeDefined();
    });

    it('should set language correctly gameService.setLanguage("de")', function () {
        gameService.setLanguage('de');
        expect(gameService.getLanguage()).toBe('de');
    });

    it('should generate board dice gameService.generateBoardDice', function () {
        var boardDice = gameService.generateBoardDice();

        expect(boardDice.firstRow).toBeDefined();
        expect(boardDice.secondRow).toBeDefined();
        expect(boardDice.thirdRow).toBeDefined();
        expect(boardDice.forthRow).toBeDefined();

        expect(boardDice.firstRow.applyDisable).toBeDefined();
        expect(boardDice.secondRow.applyDisable).toBeDefined();
        expect(boardDice.thirdRow.applyDisable).toBeDefined();
        expect(boardDice.forthRow.applyDisable).toBeDefined();

        expect(boardDice.firstRow.applyEnableAll).toBeDefined();
        expect(boardDice.secondRow.applyEnableAll).toBeDefined();
        expect(boardDice.thirdRow.applyEnableAll).toBeDefined();
        expect(boardDice.forthRow.applyEnableAll).toBeDefined();

        expect(boardDice.firstRow.chars.length).toBe(4);
        expect(boardDice.secondRow.chars.length).toBe(4);
        expect(boardDice.thirdRow.chars.length).toBe(4);
        expect(boardDice.forthRow.chars.length).toBe(4);
    });

    it('should return correct coordinates gameService.calcAvailableCoords', function () {
        var coord = gameService.calcAvailableCoords({char:"H",coords:"3x3",disabled:false,used:true,x:3,y:3});
        expect(coord).toEqual(["2x3", "4x3", "3x2", "3x4", "2x2", "2x4", "4x2", "4x4"]);
    });

});