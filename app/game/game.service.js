(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('gameService', gameService);

    gameService.$inject = ['$http', '$q'];
    /* @ngInject */
    function gameService($http, $q) {

        var language = 'en';

        var service = {
            generateBoardDice: generateBoardDice,
            calcAvailableCoords: calcAvailableCoords,
            setLanguage: setLanguage,
            getLanguage: getLanguage
        };

        return service;

        ///////////////

        function generateBoardDice() {

            var diceLetters = _getDiceLetters(),
                boardDice = _createBoardDiceObject();

            for (var i = 0; i < diceLetters.length; i++) {

                var randomChar = _getRandomChar(diceLetters[i]);

                if (i < 4) {
                    boardDice.firstRow.chars.push(
                        _createDiceObject(randomChar, boardDice.firstRow.chars.length + 1, 1, false, false)
                    );
                    continue;
                }

                if (i < 8) {
                    boardDice.secondRow.chars.push(
                        _createDiceObject(randomChar, boardDice.secondRow.chars.length + 1, 2, false, false)
                    );
                    continue;
                }

                if (i < 12) {
                    boardDice.thirdRow.chars.push(
                        _createDiceObject(randomChar, boardDice.thirdRow.chars.length + 1, 3, false, false)
                    );
                    continue;
                }

                boardDice.forthRow.chars.push(
                    _createDiceObject(randomChar, boardDice.forthRow.chars.length + 1, 4, false, false)
                );

            }

            return boardDice;
        }

        /*
        * Based on boggle rules we prepare all valid coordinates based on current active dice 
        */
        function calcAvailableCoords(dice) {
            return [
                (dice.x - 1) + 'x' + dice.y, //left
                (dice.x + 1) + 'x' + dice.y, //right

                dice.x + 'x' + (dice.y - 1), //up
                dice.x + 'x' + (dice.y + 1), //down

                (dice.x - 1) + 'x' + (dice.y - 1), //vertical left up
                (dice.x - 1) + 'x' + (dice.y + 1), //vertical left down

                (dice.x + 1) + 'x' + (dice.y - 1), //vertical right up
                (dice.x + 1) + 'x' + (dice.y + 1), //vertical right down
            ];
        }

        function getLanguage() {
            return language;
        }

        function setLanguage(lang) {
            language = lang;
        }

        function _createBoardDiceObject() {
            var applyDisable = function (dice, availableCoords) {
                for (var i = 0; i < this.chars.length; i++) {
                    var _dice = this.chars[i];

                    if (_dice.coords === dice.x + 'x' + dice.y) {
                        this.chars[i].used = true;
                        continue;
                    }

                    if (availableCoords.indexOf(_dice.coords) !== -1) {
                        this.chars[i].disabled = false;
                    } else {
                        this.chars[i].disabled = true;
                    }
                }

                return this;
            };

            var applyEnableAll = function () {

                for (var i = 0; i < this.chars.length; i++) {
                    this.chars[i].used = false;
                    this.chars[i].disabled = false;
                }

                return this;

            };

            return {
                firstRow: { chars: [], applyDisable: applyDisable, applyEnableAll: applyEnableAll },
                secondRow: { chars: [], applyDisable: applyDisable, applyEnableAll: applyEnableAll },
                thirdRow: { chars: [], applyDisable: applyDisable, applyEnableAll: applyEnableAll },
                forthRow: { chars: [], applyDisable: applyDisable, applyEnableAll: applyEnableAll }
            };
        }

        function _createDiceObject(char, x, y, disabled, used) {
            return {
                char: char,
                x: x,
                y: y,
                coords: x + 'x' + y,
                disabled: disabled,
                used: used
            };
        }

        /*
        * Returns prepared dice letters
        */
        function _getDiceLetters() {
            // It seems that letters are different for french 
            // https://boardgamegeek.com/thread/118945/differences-between-different-versions
            if (getLanguage() === 'fr') {
                return ['TOSEND', 'LETUPS', 'SHRINE', 'YELUNG', 'AIRFOX', 'HISFEE', 'IMROSA',
                    'CLEARS', 'TRIBAL', 'TIEAOA', 'VETGIN', 'OBAJMQ', 'TUNEOK', 'LUWIER', 'CMPDAE', 'NVDZEA'];
            }

            return ['AACIOT', 'ABILTY', 'ABJMOQ', 'ACELRS', 'ACEMPR', 'ADENVZ', 'AHMORS',
                'BFIORX', 'DENOSW', 'DKNOTU', 'EEFHIY', 'EGINTV', 'EGKLUY', 'EHINPS', 'ELPSTU', 'GILRUW'];
        }

        /*
        * Generates a random character
        * In case of 'Q' replace it with 'Qu' 
        * (From boggle rules because Q is nearly always followed by U in english language)
        */
        function _getRandomChar(charset) {

            var randomChar = '';

            for (var i = 0, n = charset.length; i < 1; ++i) {
                randomChar += charset.charAt(Math.floor(Math.random() * n));
            }

            if (randomChar === 'Q') {
                randomChar = 'Qu';
            }

            return randomChar;
        }
    }
})();
