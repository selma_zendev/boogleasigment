(function () {
    'use strict';

    angular
        .module('app.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$rootScope', '$q', '$state', 'playerService', 'rulesService'];
    /* @ngInject */
    function HomeController($scope, $rootScope ,$q, $state, playerService, rulesService) {
        var vm = this;
        vm.title = 'Home';

        activate();

        function activate() {

            playerService.setPlayers([]);
            playerService.setActivePlayerId(false);

            $scope.player = '';
            $scope.language = 'en';
            $scope.startGame = startGame;

            return 'Activated Home View';

            ///////////////

            function startGame(form) {

                if(!form.$valid) {
                    console.log('Invalid form');
                    return false;
                }

                playerService.setPlayers($scope.player);
                playerService.setActivePlayerId($scope.player);

                $state.go('game', {players: $scope.player, language: $scope.language});
            }
        }
    }
})();
